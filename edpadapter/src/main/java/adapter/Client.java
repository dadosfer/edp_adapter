package adapter;

import edp_side.common.Coordinate;
import edp_side.model.*;
import edp_side.model.builders.InstalacaoElectricaBuilder;
import log.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.FeatureCollection;
import org.json.JSONException;
import org.json.JSONObject;
import org.opengis.feature.simple.SimpleFeature;
import smartiot_side.model.Account;
import smartiot_side.model.Device;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Client {

    private final Logger log = new Logger("log.txt");

    private Account account;
    private final String smartIoT_account_id = "teste2";
    private final String smartIoT_account_secret = "dh3u3ptt10mdl8kiitbbd4jpe6bfk21vhp3sm06civ9kno02c21b";

    public Client() throws IOException {
        this.account = new Account();

    }

    public List<File> unzip(String zipFilePath, String destDir) throws IOException {
        log.write("[EDP] Unzip file " + zipFilePath);
        List<File> outputFiles = new ArrayList<>();

        File dir = new File(destDir);
        if (!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis, Charset.forName("CP866"));
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                log.write("[EDP] Unziping file with path " + newFile.getAbsolutePath());

                outputFiles.add(newFile);

                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();

                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            fis.close();

        } catch (IOException e) {
            log.write("[ERROR] " + e.toString());
        }
        log.write("[EDP] All files unziped");
        return outputFiles;
    }

    public String processFilesToCSV(String shpFilePath) throws IOException {
        File file = new File(shpFilePath);

        log.write("[EDP] Processing file " + file.getName());

        StringBuffer csvFile = new StringBuffer();

        try {
            Map<String, String> connect = new HashMap();
            connect.put("url", file.toURI().toString());

            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];


            FeatureSource featureSource = dataStore.getFeatureSource(typeName);
            FeatureCollection collection = featureSource.getFeatures();
            SimpleFeatureIterator iterator = (SimpleFeatureIterator) collection.features();


            try {
                while (iterator.hasNext()) {
                    SimpleFeature feature = iterator.next();
                    for (Object obj : feature.getAttributes()) {
                        csvFile.append(obj.toString()).append(",");
                    }
                    csvFile.append("\n");
                }
            } finally {
                iterator.close();

            }

        } catch (Throwable e) {
            log.write("[ERROR] " + e.toString());
        }

        log.write("[EDP] File " + file.getName() + " converted to CSV");

        return csvFile.toString();
    }

    public void parseRedeLisboa(String csvContent) throws IOException, JSONException, InterruptedException {
        String[] entities = csvContent.split("\n");

        if (!this.account.isAuthenticated()) {
            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
            log.write("[SMARTIOT] Account authenticated");
        }

        log.write("[EDP] Entities Size: " + entities.length);
        for (String entity : entities) {
            if (!entity.isEmpty()) {
                entity = entity + UUID.randomUUID().toString();


                String[] entityContent = entity.split(",");


                String entity_name = entityContent[1];

                String entity_id = entityContent[2];

                String entity_coords = entityContent[0]
                        .replaceAll("[A-Z]*", "")
                        .replaceAll("[()]", "");

                Device device;
                switch (entity_name) {
                    case "Posto de Seccionamento AT-MT":

                        if (!this.account.isAuthenticated()) {

                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa ps = new PostoDeSeccionamento("8bb18638-93b9-460e-968e-05aec01fca49", entity_name,
                                new Coordinate(Double.parseDouble(entity_coords.split(" ")[1]),
                                        Double.parseDouble(entity_coords.split(" ")[2])));

                        log.write("[EDP] Posto de seccionamento created: " + ps.toJSON().toString());

                        device = this.account.retrieveDeviceDetails(ps.getID());
                        if (device == null) {

                            log.write("[SMARTIOT] Device " + ps.getID() + " doesn't exist");
                            this.account.registerDevice(ps.toJSON());
                            log.write("[SMARTIOT] Device " + ps.getID() + " registed");

                        } else {

                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");

                        }

                        break;
                    case "Posto de Seccionamento/Corte":

                        if (!this.account.isAuthenticated()) {
                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa psc = new PostoDeSeccionamentoCorte("f23d4db8-22e5-4c11-acaa-9edd080bde04", entity_name,
                                new Coordinate(Double.parseDouble(entity_coords.split(" ")[1]),
                                        Double.parseDouble(entity_coords.split(" ")[2])));
                        log.write("[EDP] Posto de seccionamento/corte created: " + psc.toJSON().toString());
                        device = this.account.retrieveDeviceDetails(psc.getID());

                        if (device == null) {
                            log.write("[SMARTIOT] Device " + psc.getID() + " doesn't exist");
                            this.account.registerDevice(psc.toJSON());
                            log.write("[SMARTIOT] Device " + psc.getID() + " registed");

                        } else {
                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");
                        }


                        break;
                    case "Posto de Transformação":

                        if (!this.account.isAuthenticated()) {
                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa pt = new PostoDeTransformacao("230249a6-3594-4f11-b8e1-df995a9bf433", entity_name,
                                new Coordinate(Double.parseDouble(entity_coords.split(" ")[1]),
                                        Double.parseDouble(entity_coords.split(" ")[2])));
                        log.write("[EDP] Posto de Transformação created: " + pt.toJSON().toString());

                        device = this.account.retrieveDeviceDetails(pt.getID());
                        if (device == null) {
                            log.write("[SMARTIOT] Device " + pt.getID() + " doesn't exist");
                            this.account.registerDevice(pt.toJSON());
                            log.write("[SMARTIOT] Device " + pt.getID() + " registed");
                        } else {
                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");
                        }

                        break;
                    case "Subestação":

                        if (!this.account.isAuthenticated()) {
                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa subestacao = new Subestacao("e18fa65f-8662-4c64-8ebd-5a7d8d3c5c4f", entity_name,
                                new Coordinate(Double.parseDouble(entity_coords.split(" ")[1]),
                                        Double.parseDouble(entity_coords.split(" ")[2])));
                        log.write("[EDP] Subestacao created: " + subestacao.toJSON().toString());
                        device = this.account.retrieveDeviceDetails(subestacao.getID());
                        if (device == null) {
                            log.write("[SMARTIOT] Device " + subestacao.getID() + " doesn't exist");
                            this.account.registerDevice(subestacao.toJSON());
                            log.write("[SMARTIOT] Device " + subestacao.getID() + " registed");
                        } else {
                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");
                        }

                        break;
                    case "Troço AT-MT":

                        if (!this.account.isAuthenticated()) {
                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa troco = this.buildTrocoAT_MT(entity);
                        log.write("[EDP] Troço created: " + troco.toJSON().toString());


                        device = this.account.retrieveDeviceDetails(troco.getID());
                        if (device == null) {
                            log.write("[SMARTIOT] Device " + troco.getID() + " doesn't exist");
                            this.account.registerDevice(troco.toJSON());
                            log.write("[SMARTIOT] Device " + troco.getID() + " registed");
                        } else {
                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");
                        }
                        break;
                    case "IP":

                        if (!this.account.isAuthenticated()) {
                            this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                            log.write("[SMARTIOT] Account authenticated");
                        }

                        RedeLisboa ip = new IluminacaoPublica("1efcf741-82ef-47b9-8724-746cb1320529", entity_name,
                                new Coordinate(Double.parseDouble(entity_coords.split(" ")[1]),
                                        Double.parseDouble(entity_coords.split(" ")[2])));
                        log.write("[EDP] Iluminacao Publica created: " + ip.toJSON().toString());


                        device = this.account.retrieveDeviceDetails(ip.getID());


                        if (device == null) {
                            log.write("[SMARTIOT] Device " + ip.getID() + " doesn't exist");
                            this.account.registerDevice(ip.toJSON());
                            log.write("[SMARTIOT] Device " + ip.getID() + " registed");
                        } else {
                            log.write("[SMARTIOT] Device " + device.getId() + " retrieved with success");
                        }

                        break;
                }
            }
        }
    }

    private RedeLisboa buildTrocoAT_MT(String entity) {

        String[] entityContent = entity.split(",");

        String entity_name = entityContent[1];

        String entity_id = entityContent[2];

        String[] coords = entity.replaceAll(",Troço AT-MT,", "").replaceAll("[A-Z]", "")
                .replaceAll("[()]", "").split(",");

        List<Coordinate> coordinates = new ArrayList<>();
        for (int i = 0; i < coords.length; i++) {
            coordinates.add(new Coordinate(Double.parseDouble(coords[i].split(" ")[1]), Double.parseDouble(coords[i].split(" ")[2])));
        }


        return new Troco("5dc21b8a-9a7f-4333-9675-3c7f609e569f", entity_name, coordinates);
    }

    public String convertInstalacaoElectricaToCSV(String pathToFile) throws IOException, InvalidFormatException {

        StringBuffer buffer = new StringBuffer();
        File f = new File(pathToFile);
        FileInputStream file = new FileInputStream(f);
        log.write("[EDP] Processing file " + f.getName());

        Workbook workbook = WorkbookFactory.create(file);

        Sheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();


            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();

                if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                    cell.setCellValue("null");
                }

                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_BOOLEAN:
                        buffer.append(cell.getBooleanCellValue() + ",");
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            buffer.append(", " + cell.getDateCellValue().getTime());

                        } else {
                            buffer.append(", " + cell.getNumericCellValue());
                        }
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        buffer.append(", " + "null");
                        break;
                    case Cell.CELL_TYPE_STRING:
                        buffer.append(", " + cell.getStringCellValue());
                        break;
                    default:
                        buffer.append(", " + "null");
                        break;

                }

            }
            buffer.append("\n");

        }
        file.close();
        log.write("[EDP] File " + f.getName() + " converted to CSV");

        return buffer.toString();
    }

    public void parseInstalacaoElectrica(String csv, String pathToDirectory) throws IOException, InvalidFormatException, JSONException, InterruptedException {
        String[] instalacoes = csv.split("\n");

        for (int i = 1; i < instalacoes.length; i++) {
            InstalacaoElectricaBuilder builder = new InstalacaoElectricaBuilder();
            String inst = instalacoes[i].substring(1);
            if (inst.split(",").length != 6) {
                int difference = 6 - inst.split(",").length;
                for (int a = 0; a < difference; a++) {
                    inst = inst + ", null";
                }
            }

            List<String> instAttr = Arrays.asList(inst.split(","));

            builder.setNif(instAttr.get(0)).
                    setTipoInstalacao(instAttr.get(1)).
                    setNivelTensao(instAttr.get(2)).
                    setPontoEntregaID(instAttr.get(3).replaceAll(" ", "")).
                    setTipoTelecontagem(instAttr.get(4)).
                    setStatus(instAttr.get(5));

            InstalacaoElectrica ie = builder.build();
            log.write("[EDP] Instalação Eléctrica created: " + ie.toJSON().toString());


            if (!this.account.isAuthenticated()) {
                this.account.accountAuthentication(smartIoT_account_id, smartIoT_account_secret);
                log.write("[SMARTIOT] Account authenticated");
            }


            Device ieDevice = this.account.retrieveDeviceDetails(ie.getPonto_entrega_id());

            if (ieDevice == null) {
                log.write("[SMARTIOT] Device " + ie.getPonto_entrega_id() + " doesn't exist");
                ieDevice = this.account.registerDevice(ie.toJSON());
                log.write("[SMARTIOT] Device " + ie.getPonto_entrega_id() + " registed");
            } else {
                log.write("[SMARTIOT] Device " + ieDevice.getId() + " retrieved with success");

            }


            if (!ieDevice.isAuthenticated()) {
                ieDevice.deviceAuthentication();
                log.write("Device " + ieDevice.getId() + " authenticated");
            }


            this.account.createDeviceDataStream(ieDevice, "InstalacaoElectricaStatus");
            log.write("[SMARTIOT] Stream InstalacaoElectricaStatus was created on SmartIoT");

            JSONObject statusMessage = new JSONObject();
            statusMessage.put("ts", System.currentTimeMillis());
            statusMessage.put("value", "{ 'state': " + ie.getStatus() + "}");

            ieDevice.publishOnDeviceStream("InstalacaoElectricaStatus", statusMessage);
            log.write("[SMARTIOT] Message " + statusMessage.toString() + " was published by device " + ieDevice.getId()
                    + " on stream InstalacaoElectricaStatus");


            this.account.createDeviceDataStream(ieDevice, "InstalacaoElectricaTelecontagem");
            log.write("[SMARTIOT] Stream InstalacaoElectricaTelecontagem was created on SmartIoT");

            File contagemFile = new File(pathToDirectory + "/" + ie.getPonto_entrega_id() + ".xlsx");

            if (contagemFile.exists()) {
                String telecontagensContent = this.convertInstalacaoElectricaToCSV(contagemFile.getAbsolutePath());
                this.buildTelecontagens(telecontagensContent.replaceAll(" ", ""), ieDevice);
            }

        }
    }

    private void buildTelecontagens(String content, Device ie) throws IOException, JSONException {


        String[] contagens = content.split("\n");

        for (int i = 1; i < contagens.length; i++) {

            String[] countAttrs = contagens[i].substring(1).split(",");

            if (countAttrs.length == 4) {
                Telecontagem telecontagem = new Telecontagem(countAttrs[0], countAttrs[1], countAttrs[2], countAttrs[3]);
                log.write("[EDP] Telecontagem created: " + telecontagem.toString());

                if (!ie.isAuthenticated()) {
                    ie.deviceAuthentication();
                    log.write("Device " + ie.getId() + " authenticated");
                }

                ie.publishOnDeviceStream("InstalacaoElectricaTelecontagem", new JSONObject(telecontagem.toString()));
                log.write("[SMARTIOT] Message " + telecontagem.toString() + "was published by device " + ie.getId()
                        + " on stream InstalacaoElectricaTelecontagem");
            }

        }
    }

}
