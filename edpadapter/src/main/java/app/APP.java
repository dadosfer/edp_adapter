package app;

import adapter.Client;
import log.FileWriter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class APP {

    public static void main(String[] args) throws IOException, InvalidFormatException, JSONException, InterruptedException {

        FileWriter writer = new FileWriter();

        Client client = new Client();

        // /home/withus/Desktop/Work/Entidades_EMEL_EDP/Caracterizacao_Entidades/EDP_Files/AdaptTestFolder
        String folderPath = "/opt/ptin/ptin_admin/SharingCities/EDP";
        File folder = new File(folderPath);
        if (folder.isDirectory()) {

            List<File> files = Arrays.asList(folder.listFiles());

            if (!files.isEmpty()) {
                for (File zip : files) {
                    if (zip.getAbsolutePath().contains(".zip")) {
                        if(!zipExist(zip.getAbsolutePath())){
                            List<File> fls = client.unzip(zip.getAbsolutePath(), zip.getAbsolutePath().replace(".zip", ""));

                            writer.write(zip.getAbsolutePath() + "\n");


                            for (File f : fls) {
                                if (f.getName().contains("CPEs Lisboa")) {
                                    String csv = client.convertInstalacaoElectricaToCSV(f.getAbsolutePath());
                                    client.parseInstalacaoElectrica(csv, f.getParent());
                                } else {
                                    String csvContent = client.processFilesToCSV(f.getAbsolutePath());
                                    client.parseRedeLisboa(csvContent);
                                }
                            }
                        }
                    }
                }
            }


        }

    }


    public static boolean zipExist(String zipAbsolutePath) throws FileNotFoundException {
        File file = new File("edp_adapter_history.txt");
        Scanner sc = new Scanner(file);

        while(sc.hasNext()){
            if(sc.nextLine().equals(zipAbsolutePath)){
                return true;
            }
        }

        return false;
    }

}

