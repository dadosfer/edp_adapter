package edp_side.model;

import edp_side.model.builders.InstalacaoElectricaBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InstalacaoElectrica {

    private String nif;
    private String ponto_entrega_id;
    private String tipo_instalacao;
    private String nivel_tensao;
    private String tipo_telecontagem;
    private String status;

    public InstalacaoElectrica(InstalacaoElectricaBuilder builder){
        this.nif = builder.nif;
        this.ponto_entrega_id = builder.ponto_entrega_id;
        this.tipo_instalacao = builder.tipo_instalacao;
        this.nivel_tensao = builder.nivel_tensao;
        this.tipo_telecontagem = builder.tipo_telecontagem;
        this.status = builder.status;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getPonto_entrega_id() {
        return ponto_entrega_id;
    }

    public void setPonto_entrega_id(String ponto_entrega_id) {
        this.ponto_entrega_id = ponto_entrega_id;
    }

    public String getTipo_instalacao() {
        return tipo_instalacao;
    }

    public void setTipo_instalacao(String tipo_instalacao) {
        this.tipo_instalacao = tipo_instalacao;
    }

    public String getNivel_tensao() {
        return nivel_tensao;
    }

    public void setNivel_tensao(String nivel_tensao) {
        this.nivel_tensao = nivel_tensao;
    }

    public String getTipo_telecontagem() {
        return tipo_telecontagem;
    }

    public void setTipo_telecontagem(String tipo_telecontagem) {
        this.tipo_telecontagem = tipo_telecontagem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + this.ponto_entrega_id + "\",\n"+
                "\"name\": \"" + this.ponto_entrega_id +  "\"\n" +
                "}";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("id", this.ponto_entrega_id);
        jo.put("name", this.ponto_entrega_id);
        jo.put("description", this.tipo_instalacao);

        JSONObject labels = new JSONObject();
        labels.put("type_of_telecontage", this.tipo_telecontagem);

        jo.put("labels", labels);

        return jo;
    }
}
