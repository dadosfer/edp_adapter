package edp_side.model.builders;

import edp_side.model.InstalacaoElectrica;
import edp_side.model.Telecontagem;

import java.util.List;

public class InstalacaoElectricaBuilder {


    public String nif;
    public String ponto_entrega_id;
    public String tipo_instalacao;
    public String nivel_tensao;
    public String tipo_telecontagem;
    public String status;


    public InstalacaoElectricaBuilder setNif(String nif) {
        this.nif = nif;
        return this;
    }

    public InstalacaoElectricaBuilder setPontoEntregaID(String ponto_entrega_id) {
        this.ponto_entrega_id = ponto_entrega_id;
        return this;
    }

    public InstalacaoElectricaBuilder setTipoInstalacao(String tipo_instalacao) {
        this.tipo_instalacao = tipo_instalacao;
        return this;
    }

    public InstalacaoElectricaBuilder setNivelTensao(String nivel_tensao) {
        this.nivel_tensao = nivel_tensao;
        return this;
    }

    public InstalacaoElectricaBuilder setTipoTelecontagem(String tipo_telecontagem) {
        this.tipo_telecontagem = tipo_telecontagem;
        return this;
    }

    public InstalacaoElectricaBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public InstalacaoElectrica build(){
        return new InstalacaoElectrica(this);
    }

}
