package edp_side.model;

import edp_side.common.Coordinate;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class PostoDeSeccionamentoCorte implements RedeLisboa {

    private String name;
    private Coordinate coordinate;
    private String id;

    public PostoDeSeccionamentoCorte(String id, String name, Coordinate coordinate) {
        this.coordinate = coordinate;
        this.name = name;
        this.id = id;
    }

    public String getID(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public String toString() {
        return "{" +
                "'name':'" + name + '\'' +
                ", 'description': 'Coordenadas de Iluminacao Publica'" +
                ", 'labels': {" +
                "'coordinates': {" +
                "'latitude': "+ this.coordinate.getLatitude() +
                ", 'longitude': " + this.coordinate.getLongitude() +
                "}" +
                "}";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("id", id);
        jo.put("name", name);
        jo.put("description", "Coordenadas de Posto de Seccionamento/Corte");

        return jo;
    }
}