package edp_side.model;


import org.json.JSONException;
import org.json.JSONObject;

public interface RedeLisboa {

    String getName();
    void setName(String name);
    JSONObject toJSON() throws JSONException;
    String getID();
}
