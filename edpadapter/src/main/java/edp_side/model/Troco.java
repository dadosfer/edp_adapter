package edp_side.model;

import edp_side.common.Coordinate;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

public class Troco implements RedeLisboa{

    private String name;

    private List<Coordinate> coord;

    private String id;
    public Troco(String id, String name, List<Coordinate> coord) {
        this.name = name;
        this.coord = coord;
        this.id = id;
    }

    public String getID(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Coordinate> getCoord() {
        return coord;
    }

    public void setCoord(List<Coordinate> coord) {
        this.coord = coord;
    }

    @Override
    public String toString() {
        return "Troco{" +
                "name='" + name + '\'' +
                ", coord=" + coord.toString() +
                '}';
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("id", id);
        jo.put("name", name);
        jo.put("description", "Percurso");

        JSONObject labels = new JSONObject();
        labels.put("path", coord.toArray());

        jo.put("labels", labels);

        return jo;
    }

}
