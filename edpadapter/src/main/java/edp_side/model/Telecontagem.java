package edp_side.model;

import java.sql.Timestamp;

public class Telecontagem {

    private String timestamp;
    private String a_plus;
    private String ri_puls;
    private String rc_fewer;

    public Telecontagem(String timestamp, String a_plus, String ri_puls, String rc_fewer) {
        this.timestamp = timestamp;
        this.a_plus = a_plus;
        this.ri_puls = ri_puls;
        this.rc_fewer = rc_fewer;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getA_plus() {
        return a_plus;
    }

    public void setA_plus(String a_plus) {
        this.a_plus = a_plus;
    }

    public String getRi_puls() {
        return ri_puls;
    }

    public void setRi_puls(String ri_puls) {
        this.ri_puls = ri_puls;
    }

    public String getRc_fewer() {
        return rc_fewer;
    }

    public void setRc_fewer(String rc_fewer) {
        this.rc_fewer = rc_fewer;
    }

    @Override
    public String toString() {
        return "{" +
                "ts:'" + timestamp + '\'' +
                ", 'value': {" +
                "'energia_ativa': " + this.a_plus +
                ", 'energia_reativa': {" +
                "'ri+': " + this.ri_puls +
                ", 'rc-': " + this.rc_fewer +
                "}" +
                "}" +
                '}';
    }
}
