package edp_side.model;

import edp_side.common.Coordinate;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;


public class IluminacaoPublica implements RedeLisboa {

    private String name;
    private Coordinate coordinate;
    private String id;

    public IluminacaoPublica(String id, String name, Coordinate coordinate) {
        this.id = id;
        this.coordinate = coordinate;
        this.name = name;
    }

    public String getID(){
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coord) {
        this.coordinate = coord;
    }

    @Override
    public String toString() {
        return "{" +
                "'name':'" + name + '\'' +
                ", 'description': 'Coordenadas de Iluminacao Publica'" +
                ", 'data': {" +
                "'coordinates': {" +
                "'latitude': "+ this.coordinate.getLatitude() +
                ", 'longitude': " + this.coordinate.getLongitude() +
                "}" +
                "}";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("id", id);
        jo.put("name", name);
        jo.put("description", "Coordenadas de Iluminacao Publica");

        return jo;
    }

}
