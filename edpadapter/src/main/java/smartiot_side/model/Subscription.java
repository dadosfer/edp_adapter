package smartiot_side.model;

import smartiot_side.builders.SubscriptionBuilder;

public class Subscription {

    /**
     * Subscription id
     */
    private String id;

    /**
     * Subscription name
     */
    private String name;

    /**
     * Subscription description
     */
    private String description;

    /**
     * Subscription account
     */
    private Account account;

    /**
     * Subscription subscriber
     */
    private Device subscriber;

    private String device_id;

    /**
     * Subscription inputStream
     */
    private Stream inputStream;


    private Stream outputStream;

    /**
     * Subscription point of contact
     */
    private String point_of_contact;

    /**
     * Subscription state
     */
    private String state;

    /**
     * Contructor of this class
     * @param builder object that represents an implementation of builder pattern
     */
    public Subscription(SubscriptionBuilder builder){
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.account = builder.account;
        this.subscriber = builder.subscriber;
        this.inputStream = builder.inputStream;
        this.outputStream = builder.outputStream;
        this.point_of_contact = builder.point_of_contact;
        this.state = builder.state;
        this.device_id = builder.device_id;
    }


    public Stream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(Stream outputStream) {
        this.outputStream = outputStream;
    }

    public String getDevice(){
        return device_id;
    }

    /**
     * Retrieve the subscription ID
     * @return subscription id
     */
    public String getId() {
        return id;
    }

    /**
     * Set a new subscription ID
     * @param id new subscription ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieve the subscription name
     * @return subscription name
     */
    public String getName() {
        return name;
    }

    /**
     * Set a new subscription name
     * @param name new subscription name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve the subscription description
     * @return subscription description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set a new subscription description
     * @param description subscription description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieve a subscription account
     * @return subscription account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Set a new subscription account
     * @param account subscription account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Retrieve subscription subscriber
     * @return subscription subscriber
     */
    public Device getSubscriber() {
        return subscriber;
    }

    /**
     * Set a new subscription subscriber
     * @param subscriber subscription subscriber
     */
    public void setSubscriber(Device subscriber) {
        this.subscriber = subscriber;
    }

    /**
     * Retrieve the subscription inputStream
     * @return subscription inputStream
     */
    public Stream getInputStream() {
        return inputStream;
    }

    /**
     * Set a new subscription inputStream
     * @param inputStream subscription inputStream
     */
    public void setInputStream(Stream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * Retrieve the subscription point of contact
     * @return subscription point of contact
     */
    public String getPoint_of_contact() {
        return point_of_contact;
    }

    /**
     * Set a new subscription point of contact
     * @param point_of_contact new point of contact
     */
    public void setPoint_of_contact(String point_of_contact) {
        this.point_of_contact = point_of_contact;
    }

    /**
     * Retrieve the subscription state
     * @return subscription state
     */
    public String getState() {
        return state;
    }

    /**
     * Set a new subscription state
     * @param state new subscription state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * toString method implementation
     * @return instance info
     */
    @Override
    public String toString() {
        return "{" +
                "id:'" + id + '\'' +
                ", name:'" + name + '\'' +
                ", description: '" + description + '\'' +
                ", account: '" + account.getId() +'\''+
                ", subscriber: " + "'adapter'" +
                ", device_id: '"  + device_id + '\''+
                ", inputStream: '" + inputStream.getName() + '\'' +
                ", point_of_contact: '" + point_of_contact + '\'' +
                ", state: '" + state + '\'' +
                '}';
    }

}