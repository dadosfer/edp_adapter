package smartiot_side.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Stream {

    private String id;

    /**
     * Stream name
     */
    private String name;

    /**
     * Stream device
     */
    private String device;

    /**
     * List with all inputStream messages
     */
    private List<Message> messages;

    /**
     * Constructor of inputStream class
     * @param name inputStream name
     * @param device inputStream device
     */
    public Stream(String name, String device) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.device = device;
        this.messages = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieve the inputStream name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set a new inputStream name
     * @param name new inputStream name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve the inputStream device
     * @return inputStream device
     */
    public String getDevice() {
        return device;
    }

    /**
     * Set a new inputStream device
     * @param device new inputStream device
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * Retrieve all data messages published to this inputStream instance
     * @return list of data messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Set a new list of data messages
     * @param messages new list of data messages
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    /**
     * Add a new data message to the inputStream
     * @param message new data message
     */
    public void addMessage(Message message){
        this.messages.add(message);
    }

    /**
     * toString method implementation
     * @return instance actual info
     */
    @Override
    public String toString() {
        return "Stream{" +
                "name='" + name + '\'' +
                ", device=" + device +
                '}';
    }

}