package smartiot_side.model;

import org.apache.http.ParseException;

import java.util.UUID;

public class Message {

    /**
     * Unique identifier of the Message object
     */
    private String id;

    /**
     * Data message timestamp
     */
    private String timestamp;

    /**
     * Data message  value
     */
    private Object value;

    /**
     * Data message time to live
     */
    private Integer ttl;

    /**
     * Data message inputStream
     */
    private String stream;

    /**
     * Constructor of the Message class
     *
     * @param timestamp data message timestamp
     * @param value     data message value
     * @param ttl       data message time to live
     * @param stream    data message inputStream
     */
    public Message(String timestamp, Object value, Integer ttl, String stream) throws ParseException {
        this.id = UUID.randomUUID().toString();
        this.timestamp = timestamp;
        this.value = value;
        this.ttl = ttl;
        this.stream = stream;
    }

    /**
     * Retrieve the data message timestamp
     *
     * @return timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Set a new timestamp to data message
     *
     * @param timestamp new timestamp
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Retrieve the data message value
     *
     * @return data message value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Set a new data message value
     *
     * @param value new data message value
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Retrieve the data message time to live
     *
     * @return data message time to live
     */
    public Integer getTtl() {
        return ttl;
    }

    /**
     * Set a new data message time to live
     *
     * @param ttl data message time to live
     */
    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }

    /**
     * Retrieve the data message inputStream
     *
     * @return data message inputStream
     */
    public String getStream() {
        return stream;
    }

    /**
     * set a new data messagte inputStream
     *
     * @param stream new data message inputStream
     */
    public void setStream(String stream) {
        this.stream = stream;
    }

    /**
     * toString method implementation
     *
     * @return instance info
     */
    @Override
    public String toString() {
        return "{" +
                "timestamp:" + timestamp +
                ", value:" + value +
                ", ttl:" + ttl +
                ", stream:" + stream.toString() +
                '}';
    }

}