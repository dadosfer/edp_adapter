package smartiot_side.model;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import smartiot_side.builders.DeviceBuilder;
import smartiot_side.builders.SubscriptionBuilder;
import smartiot_side.common.BaseURL;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Account {

    /**
     * Account ID
     */
    private String id;

    /**
     * Account Secret
     */
    private String secret;

    /**
     * Account token
     */
    private String token;

    /**
     * All devices associated to this account instance
     */
    private Map<String, Device> devices;

    /**
     * URL for the istance of SmartIoT REST API
     */
    private BaseURL baseURL;

    /**
     * List of subscription created on this adapter instance
     */
    private List<Subscription> subscriptions;

    /**
     * Constructor of the class Account
     */
    public Account() {
        this.devices = new HashMap<>();
        this.subscriptions = new ArrayList<>();
        this.baseURL = new BaseURL();
        this.token = "";
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    /**
     * Retrieve the Account id
     *
     * @return Account id
     */
    public String getId() {
        return id;
    }

    /**
     * Set a new Account id
     *
     * @param id new account id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieve the account secret
     *
     * @return Account secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set a new account secret
     *
     * @param secret new account secret
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * Retrieve the account token
     *
     * @return Account token
     */
    public String getToken() {
        return token;
    }

    /**
     * Retrieve all devices associated to this account instance
     *
     * @return Map with all devices, the key is the device id and the value is the device details
     */
    public Map<String, Device> getDevices() {
        return devices;
    }

    /**
     * Set a new set of devices associated to this account
     *
     * @param devices new devices
     */
    public void setDevices(Map<String, Device> devices) {
        this.devices = devices;
    }

    /**
     * Implements account authentication SmartIoT API Operation
     *
     * @return true if token was retrieved successfully or false if the operation failed
     */
    public boolean accountAuthentication(String id, String secret) throws IOException, InterruptedException {

        this.id = id;
        this.secret = secret;

        String encoded = DatatypeConverter.printBase64Binary((id + ":" + secret).getBytes());
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(baseURL.URL + "accounts/token");
        post.setHeader("Authorization", "Basic " + encoded);

        HttpResponse response = client.execute(post);

        //wait 2 second for the response
        Thread.sleep(2000);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }



        if (response.getStatusLine().getStatusCode() == 200) {

            this.token = result.toString();
            return true;

        }

        return false;
    }

    public boolean isAuthenticated(){
        return !this.token.equals("");
    }

    /**
     * Implements SmartIoT API register device operation
     *
     * @param requestBody device registration info
     * @return the Device that was created
     */
    public Device registerDevice(JSONObject requestBody) throws IOException, JSONException, InterruptedException {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(baseURL.URL + "devices");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", "Bearer " + this.token);
        post.setEntity(new StringEntity(requestBody.toString(), "UTF-8"));
        HttpResponse response = client.execute(post);

        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }

        //Wait 1 second for the response
        Thread.sleep(2000);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }



        JSONObject responseBody = new JSONObject(result.toString());

        if (response.getStatusLine().getStatusCode() == 201) {

            DeviceBuilder builder = new DeviceBuilder();
            builder.setAccountId(this)
                    .setDescription(responseBody.getString("description"))
                    .setSecret(responseBody.getString("secret"))
                    .setName(responseBody.getString("name"))
                    .setID(responseBody.getString("id"));

            this.devices.put(responseBody.getString("id"), builder.build());

            return builder.build();
        }

        return null;

    }

    /**
     * Retrieve device details
     *
     * @param device_id Id of the device
     * @return Device
     */
    public Device retrieveDeviceDetails(String device_id) throws IOException, JSONException, InterruptedException {


        HttpClient client = HttpClientBuilder.create().build();
        HttpGet get = new HttpGet(this.baseURL.URL + "devices/" + device_id);
        get.setHeader("Authorization", "Bearer " + this.token);
        get.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(get);


        if(response.getStatusLine().getStatusCode() == 401 || response.getStatusLine().getStatusCode() == 500){
            return null;
        }

        //wait 1 second for the response
        Thread.sleep(2000);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        JSONObject resp = new JSONObject(result.toString());
        DeviceBuilder builder = new DeviceBuilder();
        builder.setAccountId(this)
                .setName(resp.getString("name"))
                .setDescription(resp.getString("description"))
                .setID(resp.getString("id"))
                .setSecret(resp.getString("secret"));
        Device device = builder.build();
        JSONArray streamList = resp.getJSONArray("stream_list");
        for (int i = 0; i < streamList.length(); i++) {
            JSONObject streamInfo = streamList.getJSONObject(i);
            Stream stream = new Stream(streamInfo.getString("name"), streamInfo.getString("device"));
            stream.setId(streamInfo.getString("id"));
            device.addStream(stream);
        }

        return device;
    }

    /**
     * Implements SmartIoT API update operation
     *
     * @param device      specified device to update
     * @param requestBody new device information
     * @return true if operation succeeded, false if not
     */
    public boolean updateDeviceDetails(Device device, JSONObject requestBody) throws IOException, JSONException, InterruptedException {

        HttpClient client = HttpClientBuilder.create().build();

        HttpPut put = new HttpPut(baseURL.URL + "devices/" + device.getId());
        put.setHeader("Authorization", "Bearer " + this.token);
        put.setHeader("Content-Type", "application/json");

        put.setEntity(new StringEntity(requestBody.toString()));

        HttpResponse response = client.execute(put);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);

        if (response.getStatusLine().getStatusCode() == 204) {

            Device d = this.devices.get(device.getId());

            d.setName(requestBody.getString("name"));
            d.setDescription(requestBody.getString("description"));
            return true;
        }

        return false;
    }

    /**
     * Implements SmartIoT API operation remove device
     *
     * @param device Device to remove
     * @return true if operation succeeded, false if not
     */
    public boolean deleteDevice(Device device) throws IOException, InterruptedException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpDelete delete = new HttpDelete(baseURL.URL + "devices/" + device.getId());
        delete.setHeader("Authorization", "Bearer " + this.token);
        delete.setHeader("Content-Type", "application/json");
        HttpResponse response = client.execute(delete);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);

        if (response.getStatusLine().getStatusCode() == 204) {
            this.devices.remove(device.getId());
            return true;
        }
        return false;
    }

    /**
     * Implements SmartIoT API operation create a device data inputStream
     *
     * @param device      Device
     * @param stream_name inputStream name
     * @return the Stream that was created
     */
    public Stream createDeviceDataStream(Device device, String stream_name) throws IOException, InterruptedException {

        HttpClient client = HttpClientBuilder.create().build();

        HttpPut put = new HttpPut(baseURL.URL + "devices/" + device.getId() + "/streams/" + stream_name);
        put.setHeader("Content-Type", "application/json");
        put.setHeader("Authorization", "Bearer " + this.token);

        HttpResponse response = client.execute(put);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);

        if (response.getStatusLine().getStatusCode() == 204) {
            Stream stream = new Stream(stream_name, device.getId());
            device.addStream(stream);
            return stream;
        }

        return null;
    }

    /**
     * Implements SmartIoT API Operation remove a device data inputStream
     *
     * @param stream Stream to remove
     * @return true if operation succeeded, false if not
     */
    public boolean removeDeviceStream(Stream stream) throws IOException, InterruptedException {

        HttpClient client = HttpClientBuilder.create().build();

        HttpDelete delete = new HttpDelete(baseURL.URL + "devices/" +
                stream.getDevice() + "/streams/" + stream.getName());

        delete.setHeader("Authorization", "Bearer " + this.token);

        HttpResponse response = client.execute(delete);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);

        if (response.getStatusLine().getStatusCode() == 204) {
            this.devices.get(stream.getDevice()).removeStream(stream);
            return true;
        }

        return false;

    }

    /**
     * Retrieve a Map with all streams associated to a device
     *
     * @param device Device
     * @return Map with all streams, where the key is the inputStream name and the value is all information about the specific inputStream
     */
    public Map<String, Stream> listDeviceStreams(Device device) {
        return this.devices.get(device.getId()).getStreams();
    }

    /**
     * Implements SmartIoT API Operation create subscription
     *
     * @param requestBody subscription info
     * @return Subscription object
     */
    public Subscription createSubscription(JSONObject requestBody) throws IOException, JSONException, InterruptedException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(baseURL.URL + "subscriptions");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", "Bearer " + this.token);
        post.setEntity(new StringEntity(requestBody.toString()));
        HttpResponse response = client.execute(post);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        if (response.getStatusLine().getStatusCode() == 201) {
            SubscriptionBuilder builder = new SubscriptionBuilder();
            JSONObject resp = new JSONObject(result.toString());
            builder.setAccount(this)
                    .setDescription(resp.getString("description"))
                    .setName(resp.getString("name"))
                    .setID(resp.getString("id"))
                    .setPointOfContact(resp.getString("point_of_contact"))
                    .setState(resp.getString("state"))
                    .setSubscriber(this.devices.get(resp.getString("subscriber_id")))
                    .setDevice(requestBody.getString("device_id"))
                    .setOutputStream(this.devices.get(requestBody.getString("device_id")).getStream(requestBody.getString("outputStream")))
                    .setInputStream(this.devices.get(requestBody.getString("device_id")).getStreams().get(requestBody.getString("stream")));
            Subscription subscription = builder.build();
            this.subscriptions.add(subscription);
            return subscription;
        }
        return null;
    }

    /**
     * Implements SmartIoT API operation retrieve Subscription details
     *
     * @param subscription_id Subscription ID
     * @return a String with all information about the specified Subscription
     */
    public Subscription retrieveSubscriptionDetails(String subscription_id) throws IOException, JSONException, InterruptedException {

        HttpClient client = HttpClientBuilder.create().build();

        HttpGet get = new HttpGet(baseURL.URL + "subscriptions/" + subscription_id);
        get.setHeader("Content-Type", "application/json");
        get.setHeader("Authorization", "Bearer " + this.token);

        HttpResponse response = client.execute(get);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }
        //wait 1 second for the response
        Thread.sleep(2000);
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        SubscriptionBuilder builder = new SubscriptionBuilder();
        JSONObject resp = new JSONObject(result.toString());
        builder.setAccount(this)
                .setDescription(resp.getString("description"))
                .setName(resp.getString("name"))
                .setID(resp.getString("id"))
                .setPointOfContact(resp.getString("point_of_contact"))
                .setState(resp.getString("state"))
                .setSubscriber(this.devices.get(resp.getString("subscriber_id")))
                .setInputStream(this.devices.get(resp.getString("device_id")).getStreams().get(resp.getString("inputStream")));

        Subscription subscription = builder.build();

        return subscription;


    }

    /**
     * Implements SmartIoT API Operation update subscription
     *
     * @param id      Subscription ID
     * @param reqBody new Subscripiton info
     * @return true if operation succeeded, false if not
     */
    public boolean updateSubscription(String id, JSONObject reqBody) throws IOException, InterruptedException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpPut put = new HttpPut(baseURL.URL + "subscriptions/" + id);
        put.setHeader("Content-Type", "application/json");
        put.setHeader("Authorization", "Bearer " + this.token);

        put.setEntity(new StringEntity(reqBody.toString()));

        HttpResponse response = client.execute(put);
        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }

        Thread.sleep(2000);

        if (response.getStatusLine().getStatusCode() == 204) {
            return true;
        }

        return false;

    }

    /**
     * Implements SmartIoT API operation remove subscription
     *
     * @param id Subscription ID
     * @return true if operation succeeded, false if not
     */
    public boolean removeSubscription(String id) throws IOException, InterruptedException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpDelete delete = new HttpDelete(baseURL.URL + "subscriptions/" + id);
        delete.setHeader("Content-Type", "application/json");
        delete.setHeader("Authorization", "Bearer " + this.token);
        HttpResponse response = client.execute(delete);

        if(response.getStatusLine().getStatusCode() == 401){
            this.token = "";
        }

        Thread.sleep(100);

        if (response.getStatusLine().getStatusCode() == 204) {
            return true;
        }
        return false;
    }


//    public void sendNotification(Stream inputStream, String device_id) throws IOException, JSONException, ParseException {
//        for(Subscription sub: this.subscriptions){
//            if(sub.getInputStream().getName().equals(inputStream.getName())){
//                if(sub.getDevice().equals(device_id)){
//                    Stream outputStream = sub.getOutputStream();
//                    List<Message> msgsOutPutStream = this.devices.get(device_id).readDataFromDeviceStream(outputStream);
//                    HttpClient client = HttpClientBuilder.create().build();
//                    HttpPost post = new HttpPost(sub.getPoint_of_contact());
//                    post.setHeader("Content-Type", "application/json");
//                    post.setEntity(new StringEntity(this.lastMessage(msgsOutPutStream).toString()));
//                    client.execute(post);
//                }
//            }
//        }
//    }

    public Message lastMessage(List<Message> msgs) throws ParseException, java.text.ParseException {
        Message mostRecent = msgs.get(0);
        DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss.SSS'Z'");
        int count = 0;
        for (Message msg : msgs) {
            count++;
            Date date = formatter.parse(msg.getTimestamp());
            if (date.getTime() > formatter.parse(mostRecent.getTimestamp()).getTime()) {
                mostRecent = msg;
            }
        }
        return mostRecent;
    }

    /**
     * toString() method
     *
     * @return Actual instance information
     */
    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", secret='" + secret + '\'' +
                ", token='" + token + '\'' +
                ", devices=" + devices.toString() +
                ", baseURL=" + baseURL +
                '}';
    }

}