package smartiot_side.model;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import smartiot_side.builders.DeviceBuilder;
import smartiot_side.common.BaseURL;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Device {

    /**
     * Device ID
     */
    private String id;

    /**
     * Device secret
     */
    private String secret;

    /**
     * Device token
     */
    private String token;

    /**
     * Device name
     */
    private String name;

    /**
     * Device description
     */
    private String description;

    /**
     * Device account
     */
    private Account account;

    /**
     * All streams associated to this device instance
     */
    private Map<String, Stream> streams;

    /**
     * URL for the istance of SmartIoT REST API
     */
    private BaseURL baseURL;

    /**
     * Constructor of the device class
     *
     * @param builder object that represents an implementation of builder pattern
     */
    public Device(DeviceBuilder builder) {
        this.id = builder.id;
        this.secret = builder.secret;
        this.name = builder.name;
        this.description = builder.description;
        this.account = builder.account;
        this.streams = new HashMap<>();
        this.baseURL = new BaseURL();
        this.token = "";
    }

    public Device() {
        this.streams = new HashMap<>();
        this.baseURL = new BaseURL();
        this.token = "";
    }

    /**
     * Retrieve the device id
     *
     * @return device id
     */
    public String getId() {
        return id;
    }

    /**
     * Set a new device id
     *
     * @param id new device id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieve the device secret
     *
     * @return device secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set a new device secret
     *
     * @param secret new device secret
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * Retrieve the device token
     *
     * @return device token
     */
    public String getToken() {
        return token;
    }

    /**
     * Retrieve the device name
     *
     * @return device name
     */
    public String getName() {
        return name;
    }

    /**
     * Set a new name for device
     *
     * @param name new device name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve device description
     *
     * @return device description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set a new description for device
     *
     * @param description new device description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieve the account id
     *
     * @return account id
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Retrieve a Map with all streams associated to this device instance
     *
     * @return Map where the key is the inputStream name, and the value is the inputStream info
     */
    public Map<String, Stream> getStreams() {
        return streams;
    }

    public Stream getStream(String nameStream){
        return this.streams.get(nameStream);
    }

    /**
     * Set a new set of streams associated to this device instance
     *
     * @param streams new set of streams
     */
    public void setStreams(Map<String, Stream> streams) {
        this.streams = streams;
    }

    /**
     * Add a new inputStream to the map
     *
     * @param stream new inputStream
     */
    public void addStream(Stream stream) {
        this.streams.put(stream.getName(), stream);
    }

    /**
     * Remove a specific inputStream
     *
     * @param stream inputStream to remove
     */
    public void removeStream(Stream stream) {
        this.streams.remove(stream.getName());
    }

    /**
     * Implements the SmartIoT operation device authentication
     *
     * @return true if operation succeeded, and false if not
     */
    public boolean deviceAuthentication() throws IOException {

        String encoded = DatatypeConverter.printBase64Binary((this.id + ":" + this.secret).getBytes());

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(baseURL.URL + "devices/token");
        post.setHeader("Authorization", "Basic " + encoded);

        HttpResponse response = client.execute(post);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        if (response.getStatusLine().getStatusCode() == 201) {
            this.token = result.toString();
            return true;
        }

        return false;
    }

    public boolean isAuthenticated(){
        return !this.token.equals("");
    }

    /**
     * Implements the SmartIoT operation publish data on device inputStream
     *
     * @param stream   Stream
     * @param req_body subscription info
     * @return true if operation succeeded and false if not
     */
    public boolean publishOnDeviceStream(String stream, JSONObject req_body) throws IOException, JSONException, ParseException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(this.baseURL.URL + "devices/" + this.id + "/streams/" + stream + "/value");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Authorization", "Bearer " + token);
        post.setEntity(new StringEntity(req_body.toString(), "UTF-8"));
        HttpResponse response = client.execute(post);

        if (response.getStatusLine().getStatusCode() == 202) {
            return true;
        }

        return false;
    }

    /**
     * Retrieve all inputStream values
     *
     * @param stream Stream specified
     * @return List with all data messages published on this inputStream
     */
    public List<Message> readDataFromDeviceStream(Stream stream) throws IOException, JSONException, ParseException {

        if (this.deviceAuthentication()) {
            HttpClient client = HttpClientBuilder.create().build();

            HttpGet get = new HttpGet(this.baseURL.URL + "devices/" + this.getId() + "/streams/" + stream.getName() + "/values");
            get.setHeader("Authorization", "Bearer " + this.getToken());

            HttpResponse response = client.execute(get);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            JSONObject resp = new JSONObject(result.toString());

            System.out.println(resp.toString());
            JSONArray values = resp.getJSONArray("values");
            ArrayList<Message> msgs = new ArrayList<>();
            for (int i = 0; i < values.length(); i++) {
                JSONObject msg = values.getJSONObject(i);

                Message message = new Message(msg.getString("createdAt"), msg.getString("data"), msg.getInt("timeToLive"), msg.getString("streamId"));

                msgs.add(message);
            }

            return msgs;


        }
        return null;
    }


    /**
     * Retrieve all Subscription values
     * @param subscription Subscription specified
     * @return List with all data messages of this specific subscription
     */
    public List<Message> readDataSubscriptionValue(Subscription subscription) throws IOException, JSONException {
        return subscription.getInputStream().getMessages();
    }



    /**
     * toString method
     * @return instance info
     */
    @Override
    public String toString () {

        JSONObject deviceJSON = new JSONObject();
        try {
            deviceJSON.put("id", id);
            deviceJSON.put("secret", secret);
            deviceJSON.put("name", name);
            deviceJSON.put("description", description);
            deviceJSON.put("account_id", account.getId());
            deviceJSON.put("streams", streams.toString());
        } catch (JSONException e) {

        }

        return deviceJSON.toString();
    }

}