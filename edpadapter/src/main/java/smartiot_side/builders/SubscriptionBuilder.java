package smartiot_side.builders;

import smartiot_side.model.Account;
import smartiot_side.model.Device;
import smartiot_side.model.Stream;
import smartiot_side.model.Subscription;

public class SubscriptionBuilder {
    public String id;
    public String name;
    public String description;
    public Account account;
    public Device subscriber;
    public String device_id;
    public Stream inputStream;
    public Stream outputStream;
    public String point_of_contact;
    public String state;

    public SubscriptionBuilder setDevice(String device_id) {
        this.device_id = device_id;
        return this;
    }

    /**
     * Set id for the new subscription instance
     *
     * @param id subscription id
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setID(String id) {
        this.id = id;
        return this;
    }

    /**
     * Set name for the new subscription instance
     *
     * @param name subscription name
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Set description for the new subscription instance
     *
     * @param description subscription description
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Set account for the new subscription instance
     *
     * @param account subscription account
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setAccount(Account account) {
        this.account = account;
        return this;
    }

    /**
     * Set subscriber for the new subscription instance
     *
     * @param subscriber subscription subscriber
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setSubscriber(Device subscriber) {
        this.subscriber = subscriber;
        return this;
    }

    /**
     * Set inputStream for the new subscription instance
     *
     * @param inputStream subscription inputStream
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setInputStream(Stream inputStream) {
        this.inputStream = inputStream;
        return this;
    }

    public SubscriptionBuilder setOutputStream(Stream outputStream) {
        this.outputStream = outputStream;
        return this;
    }

    /**
     * Set a point of contact for the new subscription instance
     *
     * @param point_of_contact subscription point of contact
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setPointOfContact(String point_of_contact) {
        this.point_of_contact = point_of_contact;
        return this;
    }

    /**
     * Set state for the new subscription instance
     *
     * @param state subscription state
     * @return SubscriptionBuilder
     */
    public SubscriptionBuilder setState(String state) {
        this.state = state;
        return this;
    }

    /**
     * Build an instance of subscription class
     *
     * @return subscription instance
     */
    public Subscription build() {
        return new Subscription(this);
    }
}
