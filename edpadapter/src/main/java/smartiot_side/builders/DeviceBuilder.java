package smartiot_side.builders;

import smartiot_side.model.Account;
import smartiot_side.model.Device;

public class DeviceBuilder {

    /**
     * Device id
     */
    public String id;

    /**
     * Device secret
     */
    public String secret;

    /**
     * Device name
     */
    public String name;

    /**
     * Device description
     */
    public String description;

    /**
     * Device account
     */
    public Account account;

    /**
     * Set id for the new device instance
     *
     * @param id device id
     * @return DeviceBuilder
     */
    public DeviceBuilder setID(String id) {
        this.id = id;
        return this;
    }

    /**
     * Set secret for the new device instance
     *
     * @param secret device secret
     * @return DeviceBuilder
     */
    public DeviceBuilder setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    /**
     * Set name for the new device instance
     *
     * @param name device name
     * @return DeviceBuilder
     */
    public DeviceBuilder setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Set description for the new device instance
     *
     * @param description device descripiton
     * @return DeviceBuilder
     */
    public DeviceBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Set account for the new device instance
     *
     * @param account device account
     * @return DeviceBuilder
     */
    public DeviceBuilder setAccountId(Account account) {
        this.account = account;
        return this;
    }

    /**
     * Build an instance of device class
     *
     * @return device instance
     */
    public Device build() {
        return new Device(this);
    }

}
