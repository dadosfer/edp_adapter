package log;

import java.io.BufferedWriter;
import java.io.IOException;

public class FileWriter {

    private BufferedWriter writer;

    private final String EDPHISTORY = "edp_adapter_history.txt";

    public FileWriter() throws IOException {
        this.writer = new BufferedWriter(new java.io.FileWriter(this.EDPHISTORY));
    }

    public void write(String content) throws IOException {
        this.writer.write(content);
        this.writer.flush();
    }
}
