package log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Logger {

    private BufferedWriter writer;

    public Logger(String logName) throws IOException {
        this.writer = new BufferedWriter(new FileWriter(logName));
    }

    public void write(String content) throws IOException {
        System.out.println(content);
        writer.write(new Date(System.currentTimeMillis()) + " -> " + content);
        writer.write("\n");
        writer.flush();
    }

}
