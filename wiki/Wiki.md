# EDP Adapter

A EDP fornece dados relativos à rede eléctrica. Dos dados fornecidos foram caracterizadas 3 entidades: Rede Lisboa, Instalação Eléctrica e Troço de Rede Eléctrica.

## Rede Lisboa

Esta entidade pode corresponder a um poste de iluminação pública, posto de seccionamento, posto de seccionamento/corte, posto de transformação ou uma subestação. Estas entidades são caracterizadas pelos mesmos atributos, sendo que cada uma é mapeada para um device isolado no SmartIoT, isto é, cada entidade destas corresponde a um device no SmartIoT que não possui streams associadas.


![Rede Lisboa](../images/EDP_RedeLisboa.png)


## Instalação Eléctrica

Esta entidade é caracterizada por um ponto de entrega, um tipo de instalação, o nível de tensão, o tipo de telecontagem, o status e ainda a contagem (data, A+, Ri+, Rc-). Do lado do SmartIoT esta entidade é mapeada para um device que alimenta duas streams diferentes, uma referente ao status da instalação e o outro à telecontagem.

![Instalacao Electrica](../images/EDP_InstalacaoElectrica.png)


## Troço da rede eléctrica

Esta entidade é caracterizada pelo nome e por um percurso. Visto a informação que caracteriza esta entidade ser toda estática, ela é mapeada para um dispositivo isolado do lado do SmartIoT.

![Troco](../images/EDP_Troco.png)