# EDP Adapter

Este adapter surge com a necessidade de ler e processar ficheiros fornecidos pela EDP na máquina **194.65.138.51** (SmartData). Nestes ficheiros existem dados relativos a valores recolhidos de 15 em 15 minutos ao longo de um dia, ou seja, estes ficheiros são disponibilizados uma vez por dia com dados recolhidos no intervalo referido.

![EDP Adapter](images/EDP_Adapter.png)

## Build

O adapter foi implementado com recurso à ferramenta Maven, para se fazer deploy é necessário entrar na root do projeto (edpadapter) e executar o seguinte comando:

    mvn clean install

Para gerar o executável:

    mvn clean package

E para executar o adapter basta entrar na pasta target e executar:

    java -jar edpadapter-1.0-SNAPSHOT.jar

## Packages:

### app.APP
Representa a classe main do adapter, nesta classe os ficheiros são encontrados e validados de forma a que a classe Client os possa ler e processar e as restantes classes (Client e FileWriter) são instanciadas.

### adapter.Client
Esta classe está responsável por ler e processar os ficheiros.

### log.FileWriter
Esta é a classe responsável por registar no ficheiro, **edp_adapter_history.txt**, quais os ficheiros que já foram processados de forma a evitar processar um ficheiro que já tenho sido processado.

### smartiot_side.model.Account
Esta classe representa o objecto Account do SmartIoT e implementa a maioria das operações do SmartIoT que usem o account token.

### smartiot_side.model.Device
A classe Device representa o objecto Device do SmartIoT e implementa todas as operações que usem as credenciais de um device.

### smartiot_side.model.Message
Esta classe apenas representa o objecto Message do SmartIoT.

### smartiot_side.model.Stream
Esta classe representa o objecto Stream do SmartIoT.